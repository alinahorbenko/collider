import React, {Component} from 'react'

class PageSwitcher extends Component {

    render() {
        return(
            <div id="page-switcher-container">
                <ul id="page-switcher">
                    <li className="page-switcher-active"><a>01</a></li>
                    <li><a>02</a></li>
                    <li><a>03</a></li>
                    <li><a>04</a></li>
                    <li><a>05</a></li>
                    <li><a>06</a></li>
                    <li><a>07</a></li>
                </ul>
            </div>
        )
    }
}

export default PageSwitcher;