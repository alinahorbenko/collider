import React from "react";
import ReactDOM from "react-dom";
import App from './components/app/App'
import "./css/reset.css";
import "./css/style.css";
//import './css/responsive.css';

ReactDOM.render(
  <App />,
  document.getElementById("root")
);